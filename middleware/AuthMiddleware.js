const jwt = require("jsonwebtoken");
const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");

let verifyToken = async (req, res, next) => {
  const temp = localStorage.getItem("Token");
  const token =
    req.body.token || req.query.token || req.headers["x-access-token"] || temp;

  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN);
    console.log(decoded);
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

module.exports = verifyToken;
