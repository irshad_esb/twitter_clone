const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");

const connection = require("./DbControllers");

const unfollowUser = (req, res) => {
  try {
    const { UserID1, UserID2 } = req.query; // userid1 myself userid2 oppo
    connection.query(
      `SELECT * FROM User WHERE UserID = '${UserID1}';SELECT * FROM User WHERE UserID = '${UserID2}'`,
      (err, res) => {
        if (err) throw err;
        let unfollowsql = `DELETE  FROM Follower WHERE UserID1 = '${UserID1}' AND UserID2 = '${UserID2}';UPDATE User SET FollowingCount=CASE WHEN FollowingCount IS NULL OR FollowingCount=0 THEN 0 ELSE FollowingCount-'1' END WHERE UserID='${UserID1}';UPDATE User SET FollowerCount=CASE WHEN FollowerCount IS NULL OR FollowerCount=0 THEN 0 ELSE FollowerCount-'1' END WHERE UserID='${UserID2}'`;
        connection.query(unfollowsql, (err, result) => {
          if (err) throw err;
        });
      }
    );
    res.status(200).send("Unfollow");
  } catch (error) {
    res.status(500).send("internal Server Error");
  }
};

const followUser = (req, res) => {
  try {
    const { UserID1, UserID2 } = req.query;
    connection.query(
      `SELECT * FROM User WHERE UserID = '${UserID1}';SELECT * FROM User WHERE UserID = '${UserID2}'`,
      (err, res) => {
        if (err) throw err;
        else {
          console.log("in else");
          const sql = `INSERT INTO Follower SET UserID1 = '${UserID1}',UserID2 = '${UserID2}';UPDATE User SET FollowingCount=CASE WHEN FollowingCount IS NULL OR FollowingCount=0 THEN 1 ELSE FollowingCount+'1' END WHERE UserID='${UserID1}';UPDATE User SET FollowerCount=CASE WHEN FollowerCount IS NULL OR FollowerCount=0 THEN 1 ELSE FollowerCount+'1' END WHERE UserID='${UserID2}'`;
          connection.query(sql, (err, result) => {
            if (err) throw err;
          });
        }
      }
    );
    res.status(200).send("Follow");
  } catch (error) {
    res.status(500).send("Internal Server Error");
  }
};

module.exports = {
  unfollowUser,
  followUser,
};
