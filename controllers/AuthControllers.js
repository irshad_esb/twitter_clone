// const express = require("express");
const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { config } = require("dotenv");
const notifier = require("node-notifier");
const connection = require("./DbControllers");
const path = require("path");
const { dir } = require("console");
dirname = path.join(__dirname, "../public/profile/");
// console.log(dirname);

const getRegister = (req, res) => {
  res.render("signup");
};

const getLogin = (req, res) => {
  res.render("signin");
};

const createUser = async (req, res) => {
  let sampleFile;
  let uploadPath;

  sampleFile = req.files.sampleFile;
  console.log("sample's path", sampleFile);
  uploadPath = dirname + new Date().getTime() + "_" + sampleFile.name;

  let newname = new Date().getTime() + "_" + sampleFile.name;
  // console.log("new name ", newname);

  // console.log("Upload's Path", uploadPath);
  sampleFile.mv(uploadPath, function (err) {
    if (err) return res.status(500).send(err);
  });
  username = req.body.name;
  number = req.body.number;
  email = req.body.email;
  dob = req.body.dob;
  plainpassword = req.body.password.toString();

  if (!(username && number && email && dob && plainpassword)) {
    req.status(400).send("All field is required");
  }

  var password = (await bcrypt.hash(plainpassword, 10)).toString();

  const user = {
    FullName: username,
    Email: email,
    Password: password,
    Number: number,
    Dob: dob,
    profile: newname,
  };

  connection.query(`INSERT INTO User SET ?`, user, (err, res) => {
    if (err) throw err;
  });
  res.redirect("/login");
};

const authUser = (req, res) => {
  try {
    // console.log(req.body);
    console.log(process.env.ACCESS_TOKEN);
    var localStorage = new LocalStorage("./store");
    const { email, password } = req.body;
    console.log("email and pass", email, password);
    if (email && password) {
      connection.query(
        `SELECT * FROM User WHERE Email='${email}'`,
        (err, result) => {
          if (err) throw err;
          // console.log("result....", result);
          // console.log(result[0].Password);
          bcrypt.compare(password, result[0].Password, (err, isMatched) => {
            if (err) throw err;
            if (isMatched) {
              let token = jwt.sign(
                { email: email, password: password },
                process.env.ACCESS_TOKEN,
                { expiresIn: "7d" }
              );
              console.log("tokne is: ", token);
              UserID = result[0].UserID;
              localStorage.setItem("UserID", UserID);
              localStorage.setItem("Token", token);
              // res.render("home", { token, });
              res.redirect("/home");
            } else {
              res.redirect("/login");
            }
          });
        }
      );
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  authUser,
  createUser,
  getLogin,
  getRegister,
};
