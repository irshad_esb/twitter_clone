const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");
const { config } = require("dotenv");
const notifier = require("node-notifier");

const connection = require("./DbControllers");

const getLanding = (req, res) => {
  res.render("landing");
};

const homePage = (req, res) => {
  try {
    // console.log(decoded);
    UserID = localStorage.getItem("UserID"); // userid , which is reatrive after login
    // console.log(UserID);
    let result = [];
    Tweet_limit = 20; //how many tweet is apear in home page

    sql =
      "SELECT Tweet.TweetID,Tweet.UserID,Tweet.LikeCount,Tweet.CommentCount,Tweet.RetweetCount,Tweet.Message, User.FullName,User.profile,User.UserID FROM Tweet INNER JOIN User on Tweet.UserID=User.UserID WHERE User.UserID=" +
      UserID +
      " ORDER BY Tweet.TweetID DESC";

    // console.log(sql);
    connection.query(sql, (err, result1) => {
      if (err) throw err;
      else {
        // console.log(result1);
        sql1 =
          "SELECT Tweet.TweetID,Tweet.UserID,Tweet.LikeCount,Tweet.CommentCount,Tweet.RetweetCount,Tweet.Message,User.profile, User.FullName,User.UserID FROM Tweet INNER JOIN User on Tweet.UserID=User.UserID WHERE Tweet.UserID in (SELECT UserID2 FROM Follower WHERE UserID1=" +
          UserID +
          ") ORDER BY Tweet.TweetID DESC LIMIT " +
          Tweet_limit;
        connection.query(sql1, (err, result2) => {
          if (err) throw err;
          else {
            for (var i = 0; i < result1.length; i++) {
              result[i] = result1[i];
            }
            for (var i = 0; i < result2.length; i++) {
              result[i + result1.length] = result2[i];
            }
            connection.query("SELECT * FROM `Likes`", (err, likeresult) => {
              connection.query(
                "SELECT * FROM `Retweet`",
                (err, retweetresult) => {
                  connection.query(
                    "SELECT * FROM `User` WHERE UserID = " + UserID,
                    (err, profileresult) => {
                      connection.query(
                        "SELECT * FROM `comment` ",
                        (err, commentresult) => {
                          res.render("home", {
                            data: result,
                            commentresult: commentresult,
                            likeresult: likeresult,
                            retweetresult: retweetresult,
                            UserID,
                            profileresult: profileresult,
                          });
                        }
                      );
                    }
                  );
                }
              );
            });
          }
        });
      }
    });
  } catch (error) {
    console.log(error.message);
  }
};

const newTweet = (req, res) => {
  try {
    let sql = req.query.sql;
    const UserID = localStorage.getItem("UserID");

    connection.query(sql, (err, result) => {
      if (err) throw err;
      else {
        console.log("tweet added");
        res.redirect("/home");
      }
    });
  } catch (error) {
    console.log(error.message);
  }
};

const writeComment = (req, res) => {
  let UserID = localStorage.getItem("UserID");
  // console.log(UserID);
  // console.log(req.body.comment);
  // console.log(req.body.tweetid);
  sql1 =
    'INSERT INTO `comment`(`CommentMsg`, `UserID`,`TweetID`) VALUES ("' +
    req.body.comment +
    '", "' +
    UserID +
    '", "' +
    req.body.tweetid +
    '")';
  // console.log(sql1);
  connection.query(sql1, (err, result) => {
    if (err) throw err;
    else {
      // console.log(result);

      res.redirect("/home");
      notifier.notify({
        title: "comment",
        message: "Comment Added..!",
      });
    }
  });
};

const logoutUser = (req, res) => {
  try {
    localStorage.setItem("UserID", "");
    localStorage.setItem("Token", "");

    res.redirect("/");
  } catch (error) {
    console.log(error.message);
  }
};

module.exports = {
  getLanding,
  homePage,
  newTweet,
  writeComment,
  logoutUser,
};
