const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");

const connection = require("./DbControllers");

const explorePage = (req, res) => {
  res.render("explore");
};

const totalUser = (req, res) => {
  UserID = localStorage.getItem("UserID");
  connection.query(
    "SELECT * FROM User WHERE UserID !=" +
      UserID +
      ";SELECT UserID2 FROM Follower WHERE UserID1 = " +
      UserID +
      "",
    (err, results) => {
      res.render("people", { data: results[0], data1: results[1], UserID });
    }
  );
};

const totalTweets = (req, res) => {
  UserID = localStorage.getItem("UserID");
  try {
    const sql =
      "SELECT Tweet.TweetID,Tweet.UserID,Tweet.Message,User.profile, User.FullName,User.UserID,Tweet.LikeCount,Tweet.CommentCount,Tweet.RetweetCount FROM Tweet INNER JOIN User on Tweet.UserID=User.UserID";

    connection.query(sql, (err, result) => {
      connection.query("SELECT * FROM Likes", (err, likeresult) => {
        connection.query("SELECT * FROM Retweet", (err, retweetresult) => {
          connection.query("SELECT * FROM `comment` ", (err, commentresult) => {
            res.render("tweets", {
              data: result,
              commentresult: commentresult,
              likeresult: likeresult,
              retweetresult: retweetresult,
              UserID,
            });
          });
        });
      });
    });
  } catch (error) {
    console.log(error.message);
  }
};

const searchUser = function (req, res) {
  let UserID = localStorage.getItem("UserID");
  let name = "'" + req.body.exploresearch + "%'";
  // console.log(UserID);
  connection.query(
    `SELECT FullName,UserID,profile FROM User WHERE FullName LIKE ${name}`,
    function (err, result) {
      if (err) throw err;
      // console.log(result);
      // res.render("search", { result });
      res.render("search", { data: result, UserID: UserID });
    }
  );
};
module.exports = {
  explorePage,
  totalUser,
  totalTweets,
  searchUser,
};
