const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");

const connection = require("./DbControllers");

const increaseLike = function (req, res) {
  let TweetID = req.query.TweetID;
  let UserID = localStorage.getItem("UserID");
  // console.log(UserID);

  let sql =
    " INSERT INTO `Likes`(`TweetID`, `UserID`) VALUES (" +
    TweetID +
    "," +
    UserID +
    ")";
  connection.query(sql, (err, result) => {
    // console.log(sql);
  });
  let sql2 =
    "UPDATE `Tweet` SET `LikeCount`= `LikeCount` + 1 WHERE TweetID = " +
    TweetID;
  connection.query(sql2, (err, result) => {
    // console.log(sql2);
  });
};

const decreaseLike = function (req, res) {
  let TweetID = req.query.TweetID;
  let UserID = localStorage.getItem("UserID");
  // console.log(UserID);

  let sql4 =
    " DELETE FROM `Likes` WHERE TweetID = " +
    TweetID +
    " AND UserID = " +
    UserID;
  connection.query(sql4, (err, result) => {
    // console.log(sql4);
  });

  let sql3 =
    "UPDATE `Tweet` SET `LikeCount`= `LikeCount` - 1 WHERE TweetID = " +
    TweetID;
  connection.query(sql3, (err, result) => {
    // console.log(sql3);
  });
};

const increaseRetweet = function (req, res) {
  let TweetID = req.query.TweetID;
  let UserID = localStorage.getItem("UserID");
  // console.log(UserID);

  let sql =
    " INSERT INTO `Retweet`(`TweetID`, `UserID`) VALUES (" +
    TweetID +
    "," +
    UserID +
    ")";
  connection.query(sql, (err, result) => {
    // console.log(sql);
  });
  let sql2 =
    "UPDATE `Tweet` SET `RetweetCount`= `RetweetCount` + 1 WHERE TweetID = " +
    TweetID;
  connection.query(sql2, (err, result) => {
    // console.log(sql2);
  });
};

const decreaseRetweet = function (req, res) {
  let TweetID = req.query.TweetID;
  let UserID = localStorage.getItem("UserID");
  console.log(UserID);

  let sql4 =
    " DELETE FROM `Retweet` WHERE TweetID = " +
    TweetID +
    " AND UserID = " +
    UserID;
  connection.query(sql4, (err, result) => {
    // console.log(sql4);
  });

  let sql3 =
    "UPDATE `Tweet` SET `RetweetCount`= `RetweetCount` - 1 WHERE TweetID = " +
    TweetID;
  connection.query(sql3, (err, result) => {
    // console.log(sql3);
  });
};

const commentCount = (req, res) => {
  console.log("in comment inc");
  // console.log(req.query);
  TweetID = req.query.TweetID;
  console.log("tweetid ............." + TweetID);
  // UserID=req.query.UserID;
  const sql =
    "UPDATE `Tweet` SET `CommentCount`= `CommentCount` +1 WHERE TweetID = " +
    TweetID;
  connection.query(sql, (err, result) => {
    // console.log(sql);
    console.log("comment count increase");
  });
};

module.exports = {
  increaseLike,
  decreaseLike,
  increaseRetweet,
  decreaseRetweet,
  commentCount,
};
