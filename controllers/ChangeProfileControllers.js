const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");
const path = require("path");
const connection = require("./DbControllers");
dirname = path.join(__dirname, "../public/profile/");
const editProfile = (req, res) => {
  try {
    let UserID = localStorage.getItem("UserID");
    // console.log("uid" + id);
    (sql = "SELECT * FROM User where UserID =" + UserID),
      connection.query(sql, (err, result) => {
        // console.log(result);
        res.render("edit", {
          result: result,
          UserID: UserID,
        });
      });
  } catch (err) {
    console.log(err);
  }
};

const updateProfile = (req, res) => {
  try {
    let sampleFile;
    let uploadPath;

    sampleFile = req.files.sampleFile;
    // console.log("sample's path", sampleFile);
    uploadPath = dirname + new Date().getTime() + "_" + sampleFile.name;

    let newname = new Date().getTime() + "_" + sampleFile.name;
    // console.log("new name ", newname);

    console.log("Upload's Path", uploadPath);
    sampleFile.mv(uploadPath, function (err) {
      if (err) return res.status(500).send(err);
    });
    // console.log("in");
    UserID = localStorage.getItem("UserID");
    // console.log(req.body);
    let sql1 =
      "update User set Email='" +
      req.body.email +
      "',FullName ='" +
      req.body.name +
      "', Number='" +
      req.body.number +
      "',Dob='" +
      req.body.dob +
      "',profile='" +
      newname +
      "' where UserID=" +
      UserID;
    connection.query(sql1, (err, result) => {
      if (err) throw err;
      // console.log(result);
      res.redirect("/profile");
    });
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  editProfile,
  updateProfile,
};
