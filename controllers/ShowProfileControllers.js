const { LocalStorage } = require("node-localstorage");
var localStorage = new LocalStorage("./store");

const connection = require("./DbControllers");

const getUser = (req, res) => {
  try {
    let UserID = localStorage.getItem("UserID");
    console.log("id" + UserID);
    (sql = `SELECT * FROM User where UserID = '${UserID}'`),
      connection.query(sql, (err, result) => {
        // console.log(result);
        connection.query(
          `SELECT Tweet.Message,Tweet.LikeCount,Tweet.CommentCount,Tweet.RetweetCount,Tweet.TweetID FROM Tweet WHERE UserID = '${UserID}'`,
          (err, tweets) => {
            connection.query(
              `SELECT COUNT(Message) AS TweetCount FROM Tweet WHERE UserID='${UserID}'`,
              (err, tweetcount) => {
                connection.query(
                  `SELECT COUNT(DISTINCT UserID2) AS FOLLOWERCOUNT FROM Follower WHERE UserID1 = '${UserID}'`,
                  (err, followercount) => {
                    connection.query(
                      "SELECT * FROM `Likes`",
                      (err, likeresult) => {
                        connection.query(
                          `SELECT Tweet.Message,Retweet.UserID,Tweet.LikeCount,Tweet.CommentCount,Tweet.RetweetCount,Tweet.TweetID,Retweet.UserID,User.FullName,User.profile FROM Tweet INNER JOIN Retweet ON Retweet.TweetID = Tweet.TweetID INNER JOIN User ON Tweet.UserID = User.UserID  WHERE User.UserID= '${UserID}' or Retweet.UserID= '${UserID}'`,
                          (err, retweetresult) => {
                            connection.query(
                              "SELECT * FROM `comment` ",
                              (err, commentresult) => {
                                res.render("profile", {
                                  result: result,
                                  commentresult: commentresult,
                                  followercount: followercount[0].FOLLOWERCOUNT,
                                  Tweet: tweets,
                                  TweetCount: tweetcount,
                                  likeresult: likeresult,
                                  retweetresult: retweetresult,
                                  UserID,
                                });
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      });
  } catch (err) {
    console.log(err);
  }
};

const getOtherUser = (req, res) => {
  try {
    let mainID = localStorage.getItem("UserID");
    let UserID = req.query.UserID;
    (sql = `SELECT * FROM User where UserID = '${UserID}'`),
      connection.query(sql, (err, result) => {
        connection.query(
          `SELECT UserID2 FROM Follower WHERE UserID1='${mainID}'`,
          (err, followid) => {
            connection.query(
              `SELECT Tweet.Message,Tweet.LikeCount,Tweet.CommentCount,Tweet.RetweetCount,Tweet.TweetID FROM Tweet WHERE UserID = '${UserID}'`,
              (err, tweets) => {
                connection.query(
                  `SELECT COUNT(Message) AS TweetCount FROM Tweet WHERE UserID='${UserID}'`,
                  (err, tweetcount) => {
                    connection.query(
                      `SELECT COUNT(DISTINCT UserID2) AS FOLLOWERCOUNT FROM Follower WHERE UserID1 = '${UserID}'`,
                      (err, followercount) => {
                        connection.query(
                          "SELECT * FROM `comment` ",
                          (err, commentresult) => {
                            console.log(result);
                            res.render("other_profile", {
                              result: result,
                              UserID: UserID,
                              commentresult: commentresult,
                              mainID: mainID,
                              TweetCount: tweetcount[0].TweetCount,
                              Tweet: tweets,
                              followercount: followercount[0].FOLLOWERCOUNT,
                              FollowID: followid,
                            });
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      });
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  getUser,
  getOtherUser,
};
