const express = require("express");
const path = require("path");
const port = process.env.PORT || 5000;
const fileUpload = require("express-fileupload");

require("dotenv").config();
var app = express();

const connection = require("./controllers/DbControllers");
const auth = require("./routes/AuthRoutes");

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

app.use(fileUpload());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(path.join(__dirname, "/public")));
app.use("/", auth);

connection.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});

app.listen(port, () => {
  console.log(`Server is up on http://127.0.0.1:${port}`);
});
