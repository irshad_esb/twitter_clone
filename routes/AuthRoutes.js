const express = require("express");
const router = express.Router();

const verifyToken = require("../middleware/AuthMiddleware");

const {
  authUser,
  createUser,
  getLogin,
  getRegister,
} = require("../controllers/AuthControllers");

const {
  getUser,
  getOtherUser,
} = require("../controllers/ShowProfileControllers");

const {
  increaseLike,
  decreaseLike,
  increaseRetweet,
  decreaseRetweet,
  commentCount,
} = require("../controllers/ActivityControllers");

const {
  unfollowUser,
  followUser,
} = require("../controllers/FollowControllers");

const {
  editProfile,
  updateProfile,
} = require("../controllers/ChangeProfileControllers");

const {
  explorePage,
  totalUser,
  totalTweets,
  searchUser,
} = require("../controllers/ExploreControllers");

const {
  getLanding,
  homePage,
  newTweet,
  logoutUser,
  writeComment,
} = require("../controllers/HomeControllers");

router.get("/", getLanding);

router.get("/register", getRegister);

router.get("/login", getLogin);

router.post("/register", createUser);

router.post("/login", authUser);

router.post("/addcomment", verifyToken, writeComment);

router.get("/commentInc", verifyToken, commentCount);

router.get("/profile", verifyToken, getUser);

router.get("/other_profile", verifyToken, getOtherUser);

router.get("/like", verifyToken, increaseLike);

router.get("/unlike", verifyToken, decreaseLike);

router.get("/retweet", verifyToken, increaseRetweet);

router.get("/unretweet", verifyToken, decreaseRetweet);

router.get("/NewTweet", verifyToken, newTweet);

router.get("/unfollow", verifyToken, unfollowUser);

router.get("/follow", verifyToken, followUser);

router.get("/edit", verifyToken, editProfile);

router.post("/update", verifyToken, updateProfile);

router.get("/explore ", verifyToken, explorePage);

router.get("/people", verifyToken, totalUser);

router.get("/tweets", verifyToken, totalTweets);

router.post("/search", verifyToken, searchUser);

router.get("/logout", verifyToken, logoutUser);

router.get("/home", verifyToken, homePage);

router.all("*", (req, res) => {
  try {
    res.render("error");
  } catch (err) {
    console.log(err.message);
  }
});

module.exports = router;
